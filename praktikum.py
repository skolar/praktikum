#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  modul praktikum.py
#  
#  Copyright 2017 Štěpán Kolář
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sys
from numpy import *
from sympy import *



def grad(f, x): # gradient funkce f podle promennych x
	N=len(x)
	g = array([ diff(f,x[n]) for n in range(N)])
	return g


def odch(f, x, sig): # smerodatna odchylka funcke f podle promennych x s odchylkami sig
	N=len(x)
	if N!=len(sig):
		print 'pocet promennych a pocet odchylek se lisi'
		return
	g=grad(f,x)
	y=0
	for n in range(N):
		y+=(g[n]*sig[n])**2
	return sqrt(y)


def odch2(f, x):
	N=len(x)
	sig = [Symbol('\sigma_{'+str(x[n])+'}') for n in range(N)]
	return odch(f, x, sig)


def printodch(odch, f, x, sig, po):
	print '\sigma_{', odch, '} = ', latex(odch(f,x,sig)), po
	return


def printodch2(odch, f, x, po):
	print '\sigma_{', odch, '} = ', latex(odch2(f,x)), po
	return