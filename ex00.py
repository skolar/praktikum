from sympy import *
x, y, z=symbols('x y z')
sx, sy, sz=symbols('\sigma_x \sigma_y \sigma_z')
r=sqrt(x**2+y**2+z**2)
sr=sqrt((diff(r, x)*sx)**2 + (diff(r, y)*sy)**2 + (diff(r, z)*sz)**2)
print '\sigma_r  = ', latex(sr)
